# rsync-backup

Backup using Rsync

Script for backup using rsync.\
I have two branded external hard drives 
(Samsung and Seagate) and another one
in a generic usb hdd case (NoLabel01).\
Also at the script i use an external file 
called exclude-file.txt that i reference 
folders that i don't want included at the
backup.\
Feel free to adapt for your needs.
